<?php
    if ($is_insert) {
        if ($insert_success) {
            ?>
            <!-- Success Alert Block -->
            <div style="padding: 15px;">
              <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4>  <i class="icon fa fa-check"></i> Note!</h4>
                  <?= $message ?>
              </div>
            </div>
            <!-- END Success Alert Block -->
            <?php
        } else {
            ?>

            <!-- Danger Alert Content -->
            <div style="padding: 15px;">
              <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <?= $message ?>
              </div>
            </div>
            <!-- END Danger Alert Content -->

            <?php
        }
    }
?>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Add New <strong>Article</strong>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box">
        <div class="box-body pad">
          <form action="?r=admin/add-article" method="post">

            <div class="form-group">
              <label>English Title</label>
              <input name="Article[title_en]" type="text" class="form-control" placeholder="Article english title..." required="true">
            </div>

            <div class="form-group">
              <label>Arabic Title</label>
              <input name="Article[title_ar]" type="text" class="form-control" placeholder="Article arabic title..." required="true">
            </div>

            <div class="form-group">
              <label>English Content</label>
              <textarea name="Article[content_en]" class="textarea" placeholder="Place content here..." style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="true"></textarea>
            </div>

            <div class="form-group">
              <label>Arabic Content</label>
              <textarea name="Article[content_ar]" class="textarea" placeholder="Place content here..." style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="true"></textarea>
            </div>

            <div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>

          </form>
        </div>
      </div>
      
    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->
