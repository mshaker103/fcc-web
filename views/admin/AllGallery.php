<?php
use yii\helpers\Url;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    All Gallery
    <a href="<?= Url::to(['admin/add-gallery']) ?>" class="btn btn-default"><span class="fa fa-plus"></span> Gallery Image</a>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box">
        <div class="box-body">
          <table class="table table-bordered">

            <tr>
              <th style="width: 10px">#</th>
              <th>Image</th>
              <th style="width: 50px;">Actions</th>
            </tr>

            <?php
              $index = 0;
              $all_images = glob("../gallery/*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE);
              foreach($all_images as $image) {
            ?>

            <tr>
              <td><?= $index + 1 . '.' ?></td>
              <td>
                <img height="100px" src="<?= Url::to([$image]) ?>" alt="Gallery Image">
              </td>
              <td>
                <button type="button" href="#deleteDialog<?= $index ?>" data-toggle="modal" class="btn btn-xs btn-danger"><span class="fa fa-times"></span></button>
              </td>
            </tr>

            <?php
                $index++;
              }
            ?>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->


<?php
$index = 0;
foreach ($all_images as $image) {
    ?>

    
    <div class="example-modal">
      <div class="modal" id="deleteDialog<?= $index ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Confirm Delete</h4>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete image #<strong><?= $index + 1 ?> <?= basename($image) ?></strong>?</p>
            </div>

            <form action="<?= Url::to(['admin/all-gallery']) ?>" method="post">

                <input type="text" name="deleteFileName" value="<?= basename($image) ?>" hidden="true"/>

                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-danger">Delete</button>
                </div>

            </form>

          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </div><!-- /.example-modal -->


    

    <?php
    $index++;
}
?>
        