<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Home
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $visits_count ?></h3>
            <p>Visits</p>
          </div>
          <div class="icon">
            <i class="fa fa-eye"></i>
          </div>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $articles_count ?></h3>
            <p>Articles</p>
          </div>
          <div class="icon">
            <i class="fa fa-paperclip"></i>
          </div>
        </div>
      </div><!-- ./col -->
      
      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $news_count ?></h3>
            <p>News</p>
          </div>
          <div class="icon">
            <i class="fa fa-newspaper-o"></i>
          </div>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $clients_count ?></h3>
            <p>Clients</p>
          </div>
          <div class="icon">
            <i class="fa fa-trademark"></i>
          </div>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $clinics_count ?></h3>
            <p>Clinics</p>
          </div>
          <div class="icon">
            <i class="fa fa-hospital-o"></i>
          </div>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $departments_count ?></h3>
            <p>Departments</p>
          </div>
          <div class="icon">
            <i class="fa fa-sitemap"></i>
          </div>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $doctors_count ?></h3>
            <p>Doctors</p>
          </div>
          <div class="icon">
            <i class="fa fa-user-md"></i>
          </div>
        </div>
      </div><!-- ./col -->

      <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-primary">
          <div class="inner">
            <h3><?= $banners_count ?></h3>
            <p>Banners</p>
          </div>
          <div class="icon">
            <i class="fa fa-picture-o"></i>
          </div>
        </div>
      </div><!-- ./col -->


    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->