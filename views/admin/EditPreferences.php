<?php
    if ($is_insert) {
        if ($insert_success) {
            ?>
            <!-- Success Alert Block -->
            <div style="padding: 15px;">
              <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4>  <i class="icon fa fa-check"></i> Note!</h4>
                  <?= $message ?>
              </div>
            </div>
            <!-- END Success Alert Block -->
            <?php
        } else {
            ?>

            <!-- Danger Alert Content -->
            <div style="padding: 15px;">
              <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <?= $message ?>
              </div>
            </div>
            <!-- END Danger Alert Content -->

            <?php
        }
    }
?>


<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Edit <strong>Preferences</strong>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box">
        <div class="box-body pad">
          <form action="?r=admin/edit-preferences" method="post">

            <div class="form-group">
              <label>Show Visitors</label>
              <select name="Preferences[show_visitors]" class="form-control">
                <option value="0" <?php if($showVisitors == 0) { echo "selected"; } ?> >No</option>
                <option value="1" <?php if($showVisitors == 1) { echo "selected"; } ?> >Yes</option>
              </select>
              <!-- <input name="Preferences[show_visitors]" type="text" class="form-control" placeholder="Department english title..." required="true"> -->
            </div>

            <div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>

          </form>
        </div>
      </div>
      
    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->
