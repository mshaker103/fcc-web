<?php
    if ($is_insert) {
        if ($insert_success) {
            ?>
            <!-- Success Alert Block -->
            <div style="padding: 15px;">
              <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4>  <i class="icon fa fa-check"></i> Note!</h4>
                  <?= $message ?>
              </div>
            </div>
            <!-- END Success Alert Block -->
            <?php
        } else {
            ?>

            <!-- Danger Alert Content -->
            <div style="padding: 15px;">
              <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <?= $message ?>
              </div>
            </div>
            <!-- END Danger Alert Content -->

            <?php
        }
    }
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Add New <strong>Doctor</strong>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box">
        <div class="box-body pad">
          <form action="?r=admin/add-doctor" method="post" enctype="multipart/form-data">

            <div class="form-group">
              <label>English Name</label>
              <input name="Doctor[name_en]" type="text" class="form-control" placeholder="Doctor english title..." required="true">
            </div>

            <div class="form-group">
              <label>Arabic Name</label>
              <input name="Doctor[name_ar]" type="text" class="form-control" placeholder="Doctor arabic title..." required="true">
            </div>

            <div class="form-group">
              <label>English Job Position</label>
              <input name="Doctor[job_position_en]" type="text" class="form-control" placeholder="Doctor english job position..." required="true">
            </div>

            <div class="form-group">
              <label>Arabic Job Position</label>
              <input name="Doctor[job_position_ar]" type="text" class="form-control" placeholder="Doctor arabic job position..." required="true">
            </div>

            <div class="form-group">
              <label>English Working Hours</label>
              <textarea name="Doctor[working_hours_en]" class="textarea" placeholder="Place content here..." style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="true"></textarea>
            </div>

            <div class="form-group">
              <label>Arabic Working Hours</label>
              <textarea name="Doctor[working_hours_ar]" class="textarea" placeholder="Place content here..." style="width: 100%; height: 150px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required="true"></textarea>
            </div>

            <div class="form-group">
              <label>English Ext Doctor</label>
              <input name="Doctor[ext_doctor_en]" type="text" class="form-control" placeholder="Doctor english job position..." required="true">
            </div>

            <div class="form-group">
              <label>Arabic Ext Doctor</label>
              <input name="Doctor[ext_doctor_ar]" type="text" class="form-control" placeholder="Doctor arabic job position..." required="true">
            </div>

            <div class="form-group">
              <label>Image</label>
              <input type="file" name="image" accept="image/gif,image/jpeg,image/jpg,image/png,"> <p class="text-danger">* Recommended size is 125 x 125</p>
            </div>

            <div>
              <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>

          </form>
        </div>
      </div>
      
    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->