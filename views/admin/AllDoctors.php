<?php
use yii\helpers\Url;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    All Doctors
    <a href="<?= Url::to(['admin/add-doctor']) ?>" class="btn btn-default"><span class="fa fa-plus"></span> Doctor</a>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box">
        <div class="box-body">
          <table class="table table-bordered">
            
            <tr>
              <th style="width: 10px">#</th>
              <th>English Name</th>
              <th>Arabic Name</th>
              <th>English Job Position</th>
              <th>Arabic Job Position</th>
              <th>English Working Hours</th>
              <th>Arabic Working Hours</th>
              <th>English Ext Doctor</th>
              <th>Arabic Ext Doctor</th>
              <th>Image</th>
              <th style="width: 30px;"><span class="fa fa-list"></span></th>
            </tr>

            <?php
              $index = 0;
              foreach ($doctors as $object) {
                  ?>

                  <tr>
                      <td><?= $index + 1 . '.' ?></td>
                      <td><?= $object->name_en ?></td>
                      <td><?= $object->name_ar ?></td>
                      <td><?= $object->job_position_en ?></td>
                      <td><?= $object->job_position_ar ?></td>
                      <td><?= $object->working_hours_en ?></td>
                      <td><?= $object->working_hours_ar ?></td>
                      <td><?= $object->ext_doctor_en ?></td>
                      <td><?= $object->ext_doctor_ar ?></td>
                      <td><img height="100px" src="<?= Url::to(['../doctors/' . $object->image_name]) ?>" alt="Doctor Image"></td>

                      <td class="center">
                          <div class="btn-group">
                            <button type="button" href="#deleteDialog<?= $index ?>" data-toggle="modal" class="btn btn-xs btn-danger"><span class="fa fa-times"></span></button>
                          </div>
                      </td>
                  </tr>

                  <?php
                  $index++;
              }
            ?>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->
        

<?php
$index = 0;
foreach ($doctors as $object) {
    ?>

    
    <div class="example-modal">
      <div class="modal" id="deleteDialog<?= $index ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Confirm Delete</h4>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete <strong><?= $object->name_en ?></strong>?</p>
            </div>

            <form action="<?= Url::to(['admin/all-doctors']) ?>" method="post">

                <input type="number" name="deleteId" value="<?= $object->id ?>" hidden="true"/>

                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-danger">Delete</button>
                </div>

            </form>

          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </div><!-- /.example-modal -->


    

    <?php
    $index++;
}
?>