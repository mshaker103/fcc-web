<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Add New <strong>Client</strong>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
          
        <form action="../php/post_client.php" class="dropzone box" style="height: 400px;" method="post">
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>

    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->