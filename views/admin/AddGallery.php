<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Add New <strong>Gallery Image</strong>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
          
        <form id="my-dropzone" action="../php/post_gallery.php" class="dropzone box" style="height: 400px;" method="post">
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>

    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->