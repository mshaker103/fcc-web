<?php
use yii\helpers\Url;
?>

<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    All Clinics
    <a href="<?= Url::to(['admin/add-clinic']) ?>" class="btn btn-default"><span class="fa fa-plus"></span> Clinic</a>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">

      <div class="box">
        <div class="box-body">
          <table class="table table-bordered">
            
            <tr>
              <th style="width: 10px">#</th>
              <th>English Title</th>
              <th>Arabic Title</th>
              <th>English Content</th>
              <th>Arabic Content</th>
              <th style="width: 50px;">Actions</th>
            </tr>

            <?php
              $index = 0;
              foreach ($clinics as $object) {
                  ?>

                  <tr>
                      <td><?= $index + 1 . '.' ?></td>
                      <td><?= $object->title_en ?></td>
                      <td><?= $object->title_ar ?></td>
                      <td><?= $object->content_en ?></td>
                      <td><?= $object->content_ar ?></td>

                      <td class="center">
                          <div class="btn-group">
                            <button type="button" href="#deleteDialog<?= $index ?>" data-toggle="modal" class="btn btn-xs btn-danger"><span class="fa fa-times"></span></button>
                          </div>
                      </td>
                  </tr>

                  <?php
                  $index++;
              }
            ?>

          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

    </div><!-- /.col-->
  </div><!-- ./row -->
</section><!-- /.content -->
        

<?php
$index = 0;
foreach ($clinics as $object) {
    ?>

    
    <div class="example-modal">
      <div class="modal" id="deleteDialog<?= $index ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Confirm Delete</h4>
            </div>
            <div class="modal-body">
              <p>Are you sure you want to delete <strong><?= $object->title_en ?></strong>?</p>
            </div>

            <form action="<?= Url::to(['admin/all-clinics']) ?>" method="post">

                <input type="number" name="deleteId" value="<?= $object->id ?>" hidden="true"/>

                <div class="modal-footer">
                  <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-danger">Delete</button>
                </div>

            </form>

          </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->
    </div><!-- /.example-modal -->


    

    <?php
    $index++;
}
?>