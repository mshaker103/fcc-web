<?php
use yii\helpers\Url;
?>

<!-- Main Banner
============================================= -->
<section id="slider" class="tp-banner-container index-rev-slider clearfix">
	
	<div class="fullwidthabnner">
	
		<ul>	
		
			<?php
				$images = glob('../banners/*.{jpg,JPG,jpeg,JPEG,png,PNG}', GLOB_BRACE);
				$images = array_reverse($images);
				foreach($images as $img) {
					echo '<li data-transition="slideright">';
					echo '<img width="100%" height="auto" src="' . Url::to([$img]) . '"/>';
					echo '</li>';
				}
			?>
		
		</ul>
	</div>
	
</section>






<!-- Home Boxes
============================================= -->
<div class="row text-center no-margin">

	<div class="col-md-4 bg-default">
		
		<div class="home-box">
			<span class="glyphicon glyphicon-tint"></span>
			<h3>خدمات طبية متكامله</h3>
			<p>لطلب زيارة العلاج الطبيعي المنزلي الاتصال عبر جوال المركز / 0556334446</p>
		</div>
		
	</div>
	
	<div class="col-md-4">
		
		<div class="home-box opening-hours clearfix">
			
			<span class="glyphicon glyphicon-time"></span>
			<h3>ساعات العمل</h3>
			
			<ul class="list-unstyled">
				<li class="clearfix">
					<span>
						السبت - الخميس
					</span>
					<div class="value">
						8:00 ص حتي 2:00 ص
					</div>
				</li>
				<li class="clearfix">
					<span>
						الجمعه
					</span>
					<div class="value">
						1:00 م حتي 1:00 ص
					</div>
				</li>
			</ul>
		
		</div>
		
	</div>

	<div class="col-md-4 bg-default">
		
		<div class="home-box">
			<span class="glyphicon glyphicon-tint"></span>
			<h3>العيادات المنزلية</h3>
			<p>أسطول طبي متكامل لتلبية احتياجاتكم داخل وخارج المنطقة</p>
		</div>
		
	</div>
	
</div>


<!-- Appointment
============================================= -->
<section class="appointment-sec text-center">
	
	<div class="container">
		
		<h1>احجز موعد</h1>
		<p class="lead">الآن يمكنك حجز ميعادك اونلاين دون الحاجة للذهاب للمركز. فقط سجل بياناتك وانتظر تأكيد الحجز</p>
		
		<div class="row">
			
			<div class="col-md-6 pull-right">
				<figure><img src="<?= Url::to(['ar/images/appointment-img.jpg']) ?>" alt="image" title="Appointment image" class="img-responsive lady1"></figure>
			</div>
			
			<div class="col-md-6">
				<div class="appointment-form clearfix">
				   <div class="success" id="message-app" style="display:none;"></div>
					<form name="appoint_form" id="appoint_form" method="post" action="submit.php" onSubmit="return false">
						<input type="text" name="app_fname" id="app_fname" placeholder="الاسم الاول" onKeyPress="removeChecks();">
						<input type="text" name="app_lname" id="app_lname" placeholder="اسم العائلة" onKeyPress="removeChecks();">
						<input type="email" name="app_email_address" id="app_email_address" placeholder="البريد الالكتروني" onKeyPress="removeChecks();">
						<input type="text" name="app_phone" id="app_phone" placeholder="رقم الجوال">
						<input type="text" name="datepicker" id="datepicker" placeholder="تاريخ الحجز" onClick="removeChecks();">
						<select name="gender" id="gender">
							<option>ذكر</option>
							<option>انثي</option>
							<option>طفل</option>
						</select>
						<textarea placeholder="محتوي الرسالة" name="app_msg" id="app_msg"></textarea>
						<input type="submit" value="احجز" class="btn btn-default btn-rounded" onClick="validateAppoint();">
					</form>
				</div>
			</div>
			
		</div>
		
	</div>
	
</section>


<!-- Latest News
============================================= -->
<section class="latest-news">

	<div class="container">
	
		<div class="row">
			
			<div class="col-md-6 pull-right">
				
				<h2 class="bordered light">أحدث <span>الآخبار</span> <a href="<?= Url::to(['ar/all-news']) ?>" class="btn btn-default btn-xs pull-left">الكل</a></h2>
				
				<div class="row">

					<?php
		        		foreach($news as $news_object) {
		        			$timestamp = strtotime($news_object->created_at);
		        			$year = date('Y', $timestamp); // 1-7
							$month = date('M', $timestamp); // 1-12
							$day = date('d', $timestamp); // 1-31
		        	?>

		        	<div class="col-md-6 pull-right">
						<article class="blog-item">
							<div class="blog-thumbnail">
								<div class="blog-date"><p class="day"><?= $day ?></p><p class="monthyear"><?= $month . ' ' . $year ?></p></div>
								<img src='<?= Url::to(["../news/$news_object->image_name"]) ?>' alt="" title="News Image">
							</div>
							<div class="blog-content">
								<h4 class="blog-title" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><a href='<?= Url::to(["ar/news?id=$news_object->id"]) ?>'><?= $news_object->title_ar ?></a></h4>
								<p style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><?= $news_object->content_ar ?></p>
							</div>
						</article>	
					</div>

		        	<?php
		        		}
		        	?>
					
					
				</div>
				
			</div>
			
			
			<!-- Sidebar
			============================================= -->
			<aside class="col-md-6">
				
				<h2 class="bordered light">نصائح <span>طبية</span> <a href="<?= Url::to(['ar/all-articles']) ?>" class="btn btn-default btn-xs pull-left">الكل</a></h2>
				<div class="panel-group" id="accordion">


					<?php
						$index = 0;
		        		foreach($articles as $article) {
		        	?>

					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $index ?>">
							  <span><i class="fa fa-plus"></i></span><?= $article->title_ar ?>
							</a>
						  </h4>
						</div>
						<div id="collapse<?= $index ?>" class="panel-collapse collapse">
						  <div class="panel-body">
						   <?= $article->content_ar ?>
						  </div>
						</div>
					  </div>

		        	<?php
		        			$index++;
		        		}
		        	?>

				  
				</div>
			
			</aside>
			
		</div>
		
	</div>
	
</section>



<!-- App Store
============================================= -->
<section class="medicom-app" data-stellar-background-ratio="0.3">
	<div class="container">
		<div class="row">
			<div class="col-md-5 pull-right">
				<img src="<?= Url::to(['ar/images/mobile-hand.png']) ?>" class="app-img" alt="" title="">
			</div>
			<div class="col-md-7">
				<div class="medicom-app-content">
					<h1>تطبيق عيادات عناية المستقبل متاح الآن</h1>
					<p class="lead"><br>الآن يمكنك تحميل تطبيقنا للهواتف الذكية
					<span>متاح علي المتجر</span>
					</p>
					<ul class="list-unstyled app-buttons">
						<li><a href="#."><img src="<?= Url::to(['ar/images/app-store-btn.png']) ?>" alt="" title="App Store"></a></li>
						<li><a href="#."><img src="<?= Url::to(['ar/images/play-store-btn.png']) ?>" alt="" title="Google App"></a></li>
					</ul>
					</div>
			</div>
		</div>
	</div>
</section>




<!-- Testimonials
============================================= -->
<section class="testimonials bg-white text-center">
	
	<div class="container">
		
		<h2>عملائنا</h2>
		<div class="row">
	
			<div class="col-md-12">
			
				<div id="carousel-testimonials" class="owl-carousel slide testimonials-carousel">

					<!-- Item -->
					<?php
					$images = glob("../clients/*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE);
			        foreach($images as $img) {
			            echo '<div class="item" style="vertical-alig: middle;">';
						echo '<div class="testimonials-patient-detail">';
						echo '<img src="' . Url::to([$img]) . '" class="img-thumbnail" alt="" title="">';
						echo '</div>';
						echo '</div>';
			        }
			        ?>
					<!-- End Item -->
					
				</div>

			</div>
	
		</div>
	
	</div>
	
</section>