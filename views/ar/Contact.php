<?php
use yii\helpers\Url;
?>

<!-- Sub Page Banner
============================================= -->
<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
	
	<div class="overlay"></div>
	
	<div class="container">
		<h1 class="entry-title">اتصل بنا</h1>
		<p>ورغبة من إدارة عيادات عناية المستقبل في تقديم أفضل الخدمات التي تليق بك والسعي الحثيث إلى التطوير للأفضل فإننا نأمل إبلاغنا إذا شعرتم بتأخر خدمتكم أو كان لديكم ملاحظه حول الإجراءات المطبقة أو تقصير في التـعامل معكم فـإن إدارة العيادات تأمل منكم إبلاغها عن طريق تعبئة هذا النموذج متمنين لكم دوام الصحة و التوفيق . . .</p>
	</div>
	
</section>




<!-- Sub Page Content
============================================= -->
<div id="sub-page-content" class="clearfix">
	
	<div class="container">
		
		<h2 class="light bordered">موقعنا علي <span>الخريطة</span></h2>
		
		<div class="row">
			
			<div class="col-md-12">
				
				<!-- Map
				============================================= -->
				<script src="https://maps.googleapis.com/maps/api/js?v=3key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>
				<script src="<?= Url::to(['ar/js/map.js']) ?>"></script>
				<div class="map" id="map"></div>
				
			</div>
			
		</div>
		
		<div class="row">
			
			<div class="col-md-4">
				
				<h2 class="light bordered">get in <span>touch</span></h2>
				
				
				<!-- Get in Touch Widget
				============================================= -->
				<div class="get-in-touch-widget">
					
					<ul class="list-unstyled">
						<li><i class="fa fa-phone"></i>016 325 9900</li>
						<li><i class="fa fa-fax"></i>016 325 8226</li>
						<li><i class="fa fa-envelope"></i>صندوق بريد رقم 13286<br>بريدة 51493<br>بالمملكة العربية السعودية</li>
						<li><i class="fa fa-clock-o"></i>السبت-الخميس: 8:00 ص حتي 2:00 ص<br>الجمعه: 1:00 م حتي 1:00 ص</li>
						<li><i class="fa fa-globe"></i><a href="http://fcc.med.sa/">fcc.med.sa</a></li>
						<li>
							<div class="rw-ui-container"></div>
						</li>
					</ul>
					
				</div>
				
				
				<!-- Social
				============================================= -->
				<ul class="social-rounded">
					<li><a href="https://www.facebook.com/profile.php?id=100012029535228"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://twitter.com/fcc_clinis"><i class="fa fa-twitter"></i></a></li>
					<li><a href="https://www.instagram.com/fcc_clinis/"><i class="fa fa-instagram"></i></a></li>
					<li><a href="#."><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#."><i class="fa fa-youtube"></i></a></li>
					<li><a href="#."><i class="fa fa-linkedin"></i></a></li>
				</ul>
				
			</div>

			<div class="col-md-8">
				
				<h2 class="light bordered">ارسل <span>رسالتك</span></h2>
				
				
				<!-- Contact Form
				============================================= -->
				<div class="contact-form clearfix">
					<div id="message-contact" class="success" style="display:none;">شكرا لك. سنحاول التواصل معك في أقرب وقت</div>
					<form name="contact_form" id="contact_form" action="php/submit_contact_form.php" method="POST" onSubmit="return false;">
						<input type="text" name="fname" id="fname" placeholder="الاسم الأول" onKeyPress="removeChecks();">
						<input type="text" name="email_address" id="email_address" placeholder="البريد الالكتروني" onKeyPress="removeChecks();">
						<input type="text" name="subject" id="subject" placeholder="العنوان">
						<select name="type" id="type">
							<option value="ملاحظة">ملاحظة</option>
							<option value="شكوي">شكوي</option>
							<option value="استفسار">استفسار</option>
							<option value="مقترح">مقترح</option>
						</select>
						<textarea placeholder="محتوي الرسالة" name="msg" id="msg"></textarea>
						<input type="submit" class="btn btn-default pull-left" value="ارسال" onClick="validateContact();">
					</form>
				</div>
				
			</div>
			
		</div>
		
	</div>



</div><!--end sub-page-content-->