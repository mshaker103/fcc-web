<?php
use yii\helpers\Url;
?>

<!-- Sub Page Content
============================================= -->
<div id="sub-page-content" class="clearfix">

	<div class="container">
		
		<div class="row">
		
			<div class="col-md-12 blog-wrapper clearfix">
				
				<h2 class="bordered light"><span>الأقسام</span></h2>
				
				<article class="blog-item blog-full-width blog-detail">
					
					<div class="blog-thumbnail">
						<img alt="" src="<?= Url::to(['ar/images/ultrasound.jpg']) ?>">
					</div>

					<?php
		        		foreach($departments as $object) {
		        			echo '<div class="blog-content">';
							echo '<h3 style="direction: rtl;">'. $object->title_ar .'</h3>';
							echo '<p>'. $object->content_ar .'</p>';
							echo '</div>';
		        		}
		        	?>
		        	
				</article>
				
				
			</div>
			
		</div>
		
	</div>



</div><!--end sub-page-content-->