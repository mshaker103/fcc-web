<?php
use yii\helpers\Url;
?>

<!-- Sub Page Banner
============================================= -->
<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3" style="background: url(<?= Url::to(['ar/images/about-banner.jpg']) ?>) no-repeat center">
	
	<div class="overlay"></div>
	
	<div class="container">
		<h1 class="entry-title">عن العيادات</h1>
		<p></p>
	</div>
	
</section>



<!-- Sub Page Content
============================================= -->
<div id="sub-page-content">


	<div class="container big-font">
	
		<h2 class="light bordered main-title"><span>رسالتنا</span></h2>
    	<p class="footer-bottom-text">نحن نؤمن إيماناً كاملاً وراسخاً بضرورة المضي في رفع مقاييس الجودة للرعاية الطبية وتلبية تطلعات عملائنا من خلال أطباء و طاقم إداري فعّال و ذي جاهزية عالية و تفاني في الأداء و تجهيزات ذات تقنية متطورة.</p>

    	<h2 class="light bordered main-title"><span>مبادئنا</span></h2>
    	<ul>
    		<li>الأمانة الطبية في المقام الأول</li>
			<li>المريض هو محور الاهتمام الأول</li>
			<li>الرقي بالجو العام للعمل .</li>
			<li>التدريب و التعليم أساس الاستمرار .</li>
			<li>جو مريح و صحي للمريض .</li>
			<li>الواحد للكل و الكل للواحد .</li>
			<li>الوقاية الطبية ضد العدوى .</li>
			<li>أن نكون من أفضل المراكز الطبية في المحيط الإقليمي .</li>
    	</ul>
	
		<h2 class="light bordered main-title"><span>رسالتنا</span></h2>
    	<p class="footer-bottom-text">الاستمرار في تقديم الرعاية الطبية معتمدين على أحدث ما توصل إليه الطب و البحث العلمي وتجديد خبراتنا و التطوير المستمر للطاقم الطبي لدينا و العاملين بشكل عام ، وذلك من خلال:<br>
    	<ul>
    		<li>تقديم مستوى عالي من العناية الطبية .</li>
			<li>الرقي بالكوادر الوطنية .</li>
			<li>الاستمرار في البحث عن الجودة النوعية في الرعاية الصحية .</li>
			<li>خصوصية المريض و المهنة العالية .</li>
			<li>المرونة الإدارية.</li>
    	</ul></p>

	</div>


	<div class="height20"></div>


<!-- Team
============================================= -->
<section class="testimonials bg-white text-center">
	
	<div class="container">
		
		<div class="text-center">
			<h1 class="light">تعرف على <span>افضل الاطباء</span></h1>
		</div>
		
		<div class="height50"></div>
		
		<div id="carousel-testimonials" class="owl-carousel slide testimonials-carousel">
			

			<?php
				foreach($doctors as $object) {
					echo '<div class="team-member">';
					echo '<div class="team-thumb">';
					echo '<img src="' . Url::to(['../doctors/'. $object->image_name]) . '" class="img-rounded" alt="">';
					echo '</div>';
					echo '<h5 style="color: #552f7c;">'. $object->name_ar .'</h5>';
					echo '<h5><span>'. $object->job_position_ar .'</span></h5>';
					echo '<p class="text-center">'. $object->working_hours_ar .'</p>';
					echo '<p class="text-center">'. $object->ext_doctor_ar .'</p>';
					echo '</div>';
				}
			?>

			
		 </div>

	</div>
	
	<div class="height50"></div>
	
</section>