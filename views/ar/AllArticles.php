<!-- Articles
============================================= -->
<section class="latest-news">

	<div class="container">
	
		<div class="row">
			
			
			<!-- Sidebar
			============================================= -->
			<aside class="col-md-12">
				
				<h2 class="bordered light">نصائح <span>طبية</span></h2>
				<div class="panel-group" id="accordion">


					<?php
						$index = 0;
		        		foreach($articles as $article) {
		        	?>

					<div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $index ?>">
							  <span><i class="fa fa-plus"></i></span><?= $article->title_ar ?>
							</a>
						  </h4>
						</div>
						<div id="collapse<?= $index ?>" class="panel-collapse collapse">
						  <div class="panel-body">
						   <?= $article->content_ar ?>
						  </div>
						</div>
					  </div>

		        	<?php
		        			$index++;
		        		}
		        	?>

				  
				</div>
			
			</aside>
			
		</div>
		
	</div>
	
</section>