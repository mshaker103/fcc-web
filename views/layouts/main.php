<?php
use yii\helpers\Url;
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FCC | Admin</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?= Url::to(['bootstrap/css/bootstrap.min.css']) ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= Url::to(['dist/css/AdminLTE.min.css']) ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= Url::to(['dist/css/skins/_all-skins.min.css']) ?>">

    <link rel="stylesheet" href="<?= Url::to(['plugins/dropzone/css/dropzone.css']) ?>">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="#" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>F</b>CC</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Future </b>Care Clinics</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?= Url::to(['dist/img/avatar1.png']) ?>" class="user-image" alt="User Image">
                  <span class="hidden-xs">Admin</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="<?= Url::to(['dist/img/avatar1.png']) ?>" class="img-circle" alt="User Image">
                    <p>
                      Admin
                      <small>Future Care Clinics</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-right">
                      <form action="<?= Url::to(['admin/logout']) ?>" method="post">
                        <button type="submit" class="btn btn-default btn-flat">Sign out</button>
                      </form>
                    </div>
                  </li>
                </ul>
              </li>

            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            <li>
              <a href="<?= Url::to(['admin/home']) ?>">
                <i class="fa fa-home"></i> <span>Home</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-articles']) ?>">
                <i class="fa fa-paperclip"></i> <span>Articles</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-news']) ?>">
                <i class="fa fa-newspaper-o"></i> <span>News</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-clients']) ?>">
                <i class="fa fa-trademark"></i> <span>Clients</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-clinics']) ?>">
                <i class="fa fa-hospital-o"></i> <span>Clinics</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-departments']) ?>">
                <i class="fa fa-sitemap"></i> <span>Departments</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-doctors']) ?>">
                <i class="fa fa-user-md"></i> <span>Doctors</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-banners']) ?>">
                <i class="fa fa-picture-o"></i> <span>Banners</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/all-gallery']) ?>">
                <i class="fa fa-camera"></i> <span>Gallery</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['admin/edit-preferences']) ?>">
                <i class="fa fa-cogs"></i> <span>Preferences</span>
              </a>
            </li>

            <li>
              <a href="<?= Url::to(['ar/home']) ?>">
                <i class="fa fa-globe"></i> <span>Website</span>
              </a>
            </li>

          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        

        <?= $content ?>


      </div><!-- /.content-wrapper -->

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?= Url::to(['plugins/jQuery/jQuery-2.1.4.min.js']) ?>"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?= Url::to(['bootstrap/js/bootstrap.min.js']) ?>"></script>
    <!-- FastClick -->
    <script src="<?= Url::to(['plugins/fastclick/fastclick.min.js']) ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?= Url::to(['dist/js/app.min.js']) ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?= Url::to(['dist/js/demo.js']) ?>"></script>

    <script src="<?= Url::to(['plugins/dropzone/dropzone.min.js']) ?>"></script>
    <!-- <script src="dist/js/upload_clients.js"></script> -->

  </body>
</html>
