<?php
use yii\helpers\Url;
?>


<!DOCTYPE html>
<html lang="en" class="no-js">
  <head>
    <base href="" />
	<!-- Basic Page Needs

     ================================================== -->
	 
	 <meta charset="utf-8">
	 
	 <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
	 <link rel="icon" type="image/png" href="<?= Url::to(['en/images/favicon.png']) ?>">
	
     <title>Future Care Clinics</title>
    
     <meta name="description" content="Future Care Clinics">
    
     <meta name="keywords" content="Future Care Clinics">
    
     <meta name="author" content="araa.com.sa">

	 
	 <!-- Mobile Specific Metas
    
     ================================================== -->
    
     <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0">
    
     <meta name="format-detection" content="telephone=no">

	 
	 <!-- Web Font
	 ============================================= -->
	 <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" rel="stylesheet">
	
	
	<!-- CSS
    
     ================================================== -->
	 
	 
	<!-- Theme Color
	============================================= -->
	<link rel="stylesheet" id="color" href="<?= Url::to(['en/css/blue.css']) ?>">
    
	
	<!-- Medicom Style
	============================================= -->
    <link rel="stylesheet" href="<?= Url::to(['en/css/medicom.css']) ?>">

	
	<!-- This page
	============================================= -->
    <link href="<?= Url::to(['en/css/revolution_style.css']) ?>" rel="stylesheet">
	<link href="<?= Url::to(['en/css/settings.css']) ?>" rel="stylesheet">
	
	
	<!-- Bootstrap
	============================================= -->
    <link rel="stylesheet" href="<?= Url::to(['en/css/bootstrap.css']) ?>">

    
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

	
	
	<!-- Header Scripts
    
    ================================================== -->
	
	<script src="<?= Url::to(['en/js/modernizr-2.6.2.min.js']) ?>"></script>
	
	
	</head>
    <body class="fixed-header">
		
		
		
    
		<!-- Document Wrapper
		============================================= -->
		<div id="wrapper" class="clearfix">
    
		
		<!-- Header
		============================================= -->
		<header id="header" class="medicom-header">
		
			<!-- Top Row
			============================================= -->
			<div class="solid-row"></div>
        
			<div class="container">
				
				
				<!-- Primary Navigation
				============================================= -->
				<nav class="navbar navbar-default" role="navigation">
				
					<!-- Brand and toggle get grouped for better mobile display
					============================================= -->
					
					<div class="navbar-header">
						
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#primary-nav">
						  <span class="sr-only">Toggle navigation</span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						  <span class="icon-bar"></span>
						</button>
						
						<a class="navbar-brand" href="index.php"><img src="<?= Url::to(['en/images/logo.png']) ?>" alt="" title=""></a>
					
					</div>
				
					
					<div class="collapse navbar-collapse navbar-right" id="primary-nav">
						
						<ul class="nav navbar-nav">
							
							<li>
								<a href="<?= Url::to(['en/home']) ?>">
									<i class="fa fa-home"></i> Home
								</a>
							</li>

							<li>
								<a href="<?= Url::to(['en/clinics']) ?>">
									<i class="fa fa-hospital-o"></i> Clinics
								</a>
							</li>

							<li>
								<a href="<?= Url::to(['en/departments']) ?>">
									<i class="fa fa-sitemap"></i> Departments
								</a>
							</li>

							<li>
								<a href="<?= Url::to(['en/gallery']) ?>">
									<i class="fa fa-picture-o"></i> Gallery
								</a>
							</li>

							<li>
								<a href="<?= Url::to(['en/about']) ?>">
									<i class="fa fa-info-circle"></i> About
								</a>
							</li>

							<li>
								<a href="<?= Url::to(['en/contact']) ?>">
									<i class="fa fa-envelope"></i> Contact
								</a>
							</li>

							<li><a href="<?= Url::to(['ar/home']) ?>"><strong>العربية</strong></a></li>
						  
						</ul>
						
					</div>
					
				</nav>

			</div>

    </header>

	
	
    <div id="content-index">
    
	
		<?= $content ?>

    
    </div><!--end #content-index-->
    
    
	
		
		<!-- back to top -->
		<a href="#." class="back-to-top" id="back-to-top"><i class="fa fa-angle-up"></i></a>
	
    </div><!--end #wrapper-->


    <div class="colourfull-row" style="padding: 0;  margin: 0;"></div>

    <!-- Copyright
	============================================= -->
	<?php
		use app\models\Visit;
		use app\models\Preferences;

		$visits_count = Visit::find()->count();
        $showVisitors = Preferences::find()->where(['id' => 'show_visitors'])->one();
        if ($showVisitors->value == 1) {
	?>
		<p class="text-center" style="background:#f5f5f5; border-bottom:1px solid #e6e6e6; color:#646464; margin:0; font-size: 12px; padding:10px 0 8px;">Number of visitors: <span><?= $visits_count ?></span> </p>
	<?php
	}
	?>

    <p class="text-center" style="background:#f5f5f5; border-bottom:1px solid #e6e6e6; color:#646464; margin:0; font-size: 12px; padding:10px 0 8px;">Copyright &copy; 2016 <a href="http://araa.com.sa">Araa Media</a>. All right reserved.</p>
	
    
	
	<!-- All Javascript 
	============================================= -->
	<script src="<?= Url::to(['en/js/jquery.js']) ?>"></script>
    <script src="<?= Url::to(['en/js/bootstrap.min.js']) ?>"></script>
    <script src="<?= Url::to(['en/js/jquery.stellar.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery-ui-1.10.3.custom.js']) ?>"></script>
    <script src="<?= Url::to(['en/js/owl.carousel.js']) ?>"></script>
    <script src="<?= Url::to(['en/js/counter.js']) ?>"></script>
    <script src="<?= Url::to(['en/js/waypoints.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery.uniform.js']) ?>"></script>
    <script src="<?= Url::to(['en/js/easyResponsiveTabs.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery.fancybox.pack.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery.fancybox-media.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery.mixitup.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/forms-validation.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery.jcarousel.min.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/jquery.easypiechart.min.js']) ?>"></script>
	<script src="<?= Url::to(['en/js/scripts.js']) ?>"></script>
    
	<!-- This page
	============================================= -->
	<script src="<?= Url::to(['en/js/jquery.themepunch.plugins.min.js']) ?>"></script>			
    <script src="<?= Url::to(['en/js/jquery.themepunch.revolution.min.js']) ?>"></script>
    
	
	<script>
	
		(function () {
			
			// Revolution slider
			var revapi;
			revapi = jQuery('.fullwidthabnner').revolution(
			{
				delay:9000,
				startwidth:1920,
				startheight:720,
				hideThumbs:200,
				fullWidth:"on",
				forceFullWidth:"on"
			});
			
		})();

	</script>
	
  </body>
</html>