<?php
use yii\helpers\Url;
?>


<!-- Sub Page Banner
============================================= -->
<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3">
	
	<div class="overlay"></div>
	
	<div class="container">
		<h1 class="entry-title">Contact Us</h1>
		<p>Etharums ser quidem rerum facilis dolores nemis omnis fugats vitaes nemo minima rerums unsers sadips amets.</p>
	</div>
	
</section>




<!-- Sub Page Content
============================================= -->
<div id="sub-page-content" class="clearfix">
	
	<div class="container">
		
		<h2 class="light bordered">Get<span>Directions</span></h2>
		
		<div class="row">
			
			<div class="col-md-12">
				
				<!-- Map
				============================================= -->
				<script src="https://maps.googleapis.com/maps/api/js?v=3key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>
				<script src="<?= Url::to(['en/js/map.js']) ?>"></script>
				<div class="map" id="map"></div>
				
			</div>
			
		</div>
		
		<div class="row">
			
			<div class="col-md-8">
				
				<h2 class="light bordered">Give us a <span>Message</span></h2>
				
				
				<!-- Contact Form
				============================================= -->
				<div class="contact-form clearfix">
					<div id="message-contact" class="success" style="display:none;">Thank you! we'll contact you shortly.</div>
					<form name="contact_form" id="contact_form" action="php/submit_contact_form.php" method="POST" onSubmit="return false;">
						<input type="text" name="fname" id="fname" placeholder="First Name" onKeyPress="removeChecks();">
						<input type="text" name="email_address" id="email_address" placeholder="Email Address" onKeyPress="removeChecks();">
						<input type="text" name="subject" id="subject" placeholder="Subject">
						<select name="type" id="type">
							<option value="Note">Note</option>
							<option value="Complaint">Complaint</option>
							<option value="Enquiry">Enquiry</option>
							<option value="Suggestion">Suggestion</option>
						</select>
						<textarea placeholder="Message" name="msg" id="msg"></textarea>
						<input type="submit" class="btn btn-default" value="Submit" onClick="validateContact();">
					</form>
				</div>
				
			</div>
			
			<div class="col-md-4">
				
				<h2 class="light bordered">get in <span>touch</span></h2>
				
				
				<!-- Get in Touch Widget
				============================================= -->
				<div class="get-in-touch-widget">
					
					<ul class="list-unstyled">
						<li><i class="fa fa-phone"></i>016 325 9900</li>
						<li><i class="fa fa-fax"></i>016 325 8226</li>
						<li><i class="fa fa-envelope"></i>P.O. BOX 13286<br>Buraydeh 51493<br>Saudi Arabia</li>
						<li><i class="fa fa-clock-o"></i>Saturday-Thursday: 8:00 am till 2:00 am<br>Friday: 1:00 pm till 1:00 am</li>
						<li><i class="fa fa-globe"></i><a href="http://fcc.med.sa/">fcc.med.sa</a></li>
						<li>
							<div class="rw-ui-container"></div>
						</li>
					</ul>
					
				</div>
				
				
				<!-- Social
				============================================= -->
				<ul class="social-rounded">
					<li><a href="https://www.facebook.com/profile.php?id=100012029535228"><i class="fa fa-facebook"></i></a></li>
					<li><a href="https://twitter.com/fcc_clinis"><i class="fa fa-twitter"></i></a></li>
					<li><a href="https://www.instagram.com/fcc_clinis/"><i class="fa fa-instagram"></i></a></li>
					<li><a href="#."><i class="fa fa-google-plus"></i></a></li>
					<li><a href="#."><i class="fa fa-youtube"></i></a></li>
					<li><a href="#."><i class="fa fa-linkedin"></i></a></li>
				</ul>
				
			</div>
			
		</div>
		
	</div>



</div><!--end sub-page-content-->
