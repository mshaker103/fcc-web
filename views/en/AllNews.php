<?php
use yii\helpers\Url;
?>

<!-- Latest News
============================================= -->
<section class="latest-news">

	<div class="container">
	
		<div class="row">
			
			<div class="col-md-12">
				
				<h2 class="bordered light">Latest <span>News</span></h2>
				
				<div class="row">

					<?php
		        		foreach($news as $news_object) {
		        			$timestamp = strtotime($news_object->created_at);
		        			$year = date('Y', $timestamp); // 1-7
							$month = date('M', $timestamp); // 1-12
							$day = date('d', $timestamp); // 1-31
		        	?>

		        	<div class="col-md-6">
						<article class="blog-item">
							<div class="blog-thumbnail">
								<div class="blog-date"><p class="day"><?= $day ?></p><p class="monthyear"><?= $month . ' ' . $year ?></p></div>
								<img src="<?= Url::to(['../news/' . $news_object->image_name]) ?>" alt="" title="News Image">
							</div>
							<div class="blog-content">
								<h4 class="blog-title"><a href="<?= Url::to(['en/news?id=' . $news_object->id]) ?>"><?= $news_object->title_en ?></a></h4>
								<p><?= $news_object->content_en ?></p>
							</div>
						</article>	
					</div>

		        	<?php
		        		}
		        	?>
					
					
				</div>
				
			</div>
			
		</div>
		
	</div>
	
</section>