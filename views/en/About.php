<?php
use yii\helpers\Url;
?>

<!-- Sub Page Banner
============================================= -->
<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3" style="background: url(<?= Url::to(['en/images/about-banner.jpg']) ?>) no-repeat center">
	
	<div class="overlay"></div>
	
	<div class="container">
		<h1 class="entry-title">about us</h1>
		<p></p>
	</div>
	
</section>



<!-- Sub Page Content
============================================= -->
<div id="sub-page-content">


<div class="container big-font">
	
	<h2 class="light bordered main-title">who <span>we are</span></h2>

	<div class="row">

    	<div class="col-md-6">
        	<p class="footer-bottom-text">Future Care Clinics is the first and largest integrated in the area. Doctors from every medical specialty work together to care for patients, joined by common systems and a philosophy of "the needs of the patient come first".<br><br>
        	<strong>Mission & Value:</strong> Will provide the best care to every patient every day through integrated clinical practice.<br><br>
        	<strong>Primary Value:</strong> The needs of the patient come first.</p>
        </div>
        
        <div class="col-md-6">
			<!-- Toggle Styles
			============================================= -->
			<h5>Core Principles</h5>

			<div class="panel-group accordian-style2" id="principles">

				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title">
							<a data-toggle="collapse" data-parent="#principles" href="#principles1">
								<i class="fa fa-medkit"></i>Practice<i class="fa fa-angle-up pull-right"></i>
							</a>
						</h5>
					</div>
						
					<div id="principles1" class="panel-collapse collapse">
						<div class="panel-body">
							Practice medicine as an integrated team of compassionate, multi-disciplinary physicians,  and allied health professionals who are focused on the needs of patients from our communities and regions.
						</div>
					</div>
						
				</div>
				
				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title">
							<a data-toggle="collapse" data-parent="#principles" href="#principles2">
								<i class="fa fa-medkit"></i>Education<i class="fa fa-angle-up pull-right"></i>
							</a>
						</h5>
					</div>
						
					<div id="principles2" class="panel-collapse collapse">
						<div class="panel-body">
							Educate physicians, scientists and allied health professionals and be a dependable source of health information for our patients and the public.
						</div>
					</div>
						
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title">
							<a data-toggle="collapse" data-parent="#principles" href="#principles3">
								<i class="fa fa-medkit"></i>Mutual Respect<i class="fa fa-angle-up pull-right"></i>
							</a>
						</h5>
					</div>
						
					<div id="principles3" class="panel-collapse collapse">
						<div class="panel-body">
							Treat everyone in our diverse community with respect and dignity.
						</div>
					</div>
						
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title">
							<a data-toggle="collapse" data-parent="#principles" href="#principles4">
								<i class="fa fa-medkit"></i>Commitment to Quality<i class="fa fa-angle-up pull-right"></i>
							</a>
						</h5>
					</div>
						
					<div id="principles4" class="panel-collapse collapse">
						<div class="panel-body">
							Continuously improve all processes that support patient care and education.
						</div>
					</div>
						
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title">
							<a data-toggle="collapse" data-parent="#principles" href="#principles5">
								<i class="fa fa-medkit"></i>Work Atmosphere<i class="fa fa-angle-up pull-right"></i>
							</a>
						</h5>
					</div>
						
					<div id="principles5" class="panel-collapse collapse">
						<div class="panel-body">
							Foster teamwork, personal responsibility, integrity, innovation, trust and communication within the context of a physician-led institution.
						</div>
					</div>
						
				</div>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h5 class="panel-title">
							<a data-toggle="collapse" data-parent="#principles" href="#principles6">
								<i class="fa fa-medkit"></i>Societal Commitment<i class="fa fa-angle-up pull-right"></i>
							</a>
						</h5>
					</div>
						
					<div id="principles6" class="panel-collapse collapse">
						<div class="panel-body">
							Benefit humanity through patient care and education. Support the communities in which we live and work. Serve appropriately patients in difficult financial circumstances.
						</div>
					</div>
						
				</div>

			</div>	
		</div>

    </div>
	

</div>


<div class="height20"></div>


<!-- Team
============================================= -->
<section class="testimonials bg-white text-center">
	
	<div class="container">
		
		<div class="text-center">
			<h1 class="light">meet the <span>perfect specialists</span> team</h1>
		</div>
		
		<div class="height50"></div>
		
		<div id="carousel-testimonials" class="owl-carousel slide testimonials-carousel">
			

			<?php
				foreach($doctors as $object) {
					echo '<div class="team-member">';
					echo '<div class="team-thumb">';
					echo '<img src="' . Url::to(['../doctors/' . $object->image_name]) . '" class="img-rounded" alt="">';
					echo '</div>';
					echo '<h5 style="color: #552f7c;">'. $object->name_en .'</h5>';
					echo '<h5><span>'. $object->job_position_en .'</span></h5>';
					echo '<p class="text-center">'. $object->working_hours_en .'</p>';
					echo '<p class="text-center">'. $object->ext_doctor_en .'</p>';
					echo '</div>';
				}
			?>

			
		 </div>

	</div>
	
	<div class="height50"></div>
	
</section>