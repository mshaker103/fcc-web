<?php
use yii\helpers\Url;
?>

<!-- Sub Page Banner
============================================= -->
<section class="sub-page-banner text-center" data-stellar-background-ratio="0.3" style="background: url(<?= Url::to(['en/images/gallery-banner.jpg']) ?>) no-repeat center" >
	<div class="overlay"></div>
	<div class="container">
		<h1 class="entry-title">Gallery</h1>
	</div>
</section>



<!-- Sub Page Content
============================================= -->
<div id="sub-page-content" class="clearfix">
	
	
	
	<div class="container">
	
	
		<!-- BEGIN GALLERY SECTION -->

		<section class="gallery no-padding">
				
				<div id="Container-gallery">
					
					<ul class="four-column-gallery clearfix">
						
						<!-- Item -->
						<?php
						$images = glob('../gallery/*.{jpg,JPG,jpeg,JPEG,png,PNG}', GLOB_BRACE);
				        foreach($images as $img) {
				            echo '<li class="mix">';
							echo '<div class="gallery-item">';
							echo '<div class="gallery-item-thumb">';
							echo '<span class="overlay"></span>';
							echo '<img height="200px" src="' . Url::to([$img]) . '" alt="Gallery Item">';
							echo '<a href="' . Url::to([$img]) . '" data-fancybox-group="portfolio" class="hover-button-plus fancybox"></a>';
							echo '</div>';
							echo '<div class="gallery-item-info">';
							echo '</div>';
							echo '</div>';
							echo '</li>';
				        }
				        ?>
						<!-- End Item -->
						
					</ul>
					
				</div>

				
		</section><!-- END GALLERY SECTION -->

		
	</div>
		
			  
</div><!--end sub-page-content-->