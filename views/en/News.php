<?php
use yii\helpers\Url;
?>

<!-- Sub Page Content
============================================= -->
<div id="sub-page-content" class="clearfix">

	<div class="container">
		
		<div class="row">
		
			<div class="col-md-12 blog-wrapper clearfix">
				
				<h2 class="bordered light"><span><?= $news->title_en ?></span></h2>
				
				<article class="blog-item blog-full-width blog-detail">
					
					<div class="col-md-4">
						<div class="blog-thumbnail">
							<img alt="" src="<?= Url::to(['../news/' . $news->image_name]) ?>">
						</div>
					</div>

					<div class="col-md-8">
	        			<div class="blog-content">
							<p><?= $news->content_en ?></p>
						</div>
					</div>
		        	
				</article>
				
				
			</div>
			
		</div>
		
	</div>



</div><!--end sub-page-content-->