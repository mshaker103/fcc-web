<?php
use yii\helpers\Url;
?>


<!-- Main Banner
============================================= -->
<section id="slider" class="tp-banner-container index-rev-slider clearfix">
	
	<div class="fullwidthabnner">
	
		<ul>	
		
			<?php
				$images = glob('../banners/*.{jpg,JPG,jpeg,JPEG,png,PNG}', GLOB_BRACE);
				$images = array_reverse($images);
				foreach($images as $img) {
					echo '<li data-transition="slideright">';
					echo '<img src="' . Url::to([$img]) . '"/>';
					echo '</li>';
				}
			?>
		
		</ul>		
		
	</div>	
	
</section>






<!-- Home Boxes
============================================= -->
<div class="row text-center no-margin">

	<div class="col-md-4 bg-default">
		
		<div class="home-box">
			<span class="glyphicon glyphicon-tint"></span>
			<h3>Full Medical Services</h3>
			<p>Request to visit home physical therapy contact via IRC / mobile 0556334446</p>
		</div>
		
	</div>
	
	<div class="col-md-4">
		
		<div class="home-box opening-hours clearfix">
			
			<span class="glyphicon glyphicon-time"></span>
			<h3>Opening Hours</h3>
			<br>
			
			<ul class="list-unstyled">
				<li class="clearfix">
					<span>
						Staurday - Thursday
					</span>
					<div class="value">
						8:00 am till 2:00 am
					</div>
				</li>
				<li class="clearfix">
					<span>
						Friday
					</span>
					<div class="value">
						1:00 pm till 1:00 am
					</div>
				</li>
			</ul>
		
		</div>
		
	</div>
	
	<div class="col-md-4 bg-default">
		
		<div class="home-box">
			<span class="glyphicon glyphicon-tint"></span>
			<h3>Emergency Services</h3>
			<p>Our medical fleet is available 24 hours a day for emergency services.</p>
		</div>
		
	</div>
	
</div>


<!-- Appointment
============================================= -->
<section class="appointment-sec text-center">
	
	<div class="container">
		
		<h1>Make an appointment</h1>
		<p class="lead">Now you can reserve an appointment online without going to the clinics. Make an appointment then wait for confirmation.</p>
		
		<div class="row">
			
			<div class="col-md-6">
				<figure><img src="<?= Url::to(['en/images/appointment-img.jpg']) ?>" alt="image" title="Appointment image" class="img-responsive lady1"></figure>
			</div>
			
			<div class="col-md-6">
				<div class="appointment-form clearfix">
				   <div class="success" id="message-app" style="display:none;"></div>
					<form name="appoint_form" id="appoint_form" method="post" action="submit.php" onSubmit="return false">
						<input type="text" name="app_fname" id="app_fname" placeholder="First Name" onKeyPress="removeChecks();">
						<input type="text" name="app_lname" id="app_lname" placeholder="Last Name" onKeyPress="removeChecks();">
						<input type="email" name="app_email_address" id="app_email_address" placeholder="Email Address" onKeyPress="removeChecks();">
						<input type="text" name="app_phone" id="app_phone" placeholder="Phone No">
						<input type="text" name="datepicker" id="datepicker" placeholder="Appointment Date" onClick="removeChecks();">
						<select name="gender" id="gender">
							<option>Male</option>
							<option>Female</option>
							<option>Child</option>
						</select>
						<textarea placeholder="Message" name="app_msg" id="app_msg"></textarea>
						<input type="submit" value="submit" class="btn btn-default btn-rounded" onClick="validateAppoint();">
					</form>
				</div>
			</div>
			
		</div>
		
	</div>
	
</section>


<!-- Latest News
============================================= -->
<section class="latest-news">

	<div class="container">
	
		<div class="row">
			
			<div class="col-md-6">
				
				<h2 class="bordered light">Latest <span>News</span> <a href="<?= Url::to(['en/all-news']) ?>" class="btn btn-default btn-xs pull-right">All</a></h2>
				
				<div class="row">
					
					<?php
		        		foreach($news as $news_object) {
		        			$timestamp = strtotime($news_object["created_at"]);
		        			$year = date('Y', $timestamp); // 1-7
							$month = date('M', $timestamp); // 1-12
							$day = date('d', $timestamp); // 1-31
		        	?>

		        	<div class="col-md-6">
						<article class="blog-item">
							<div class="blog-thumbnail">
								<div class="blog-date"><p class="day"><?= $day ?></p><p class="monthyear"><?= $month . ' ' . $year ?></p></div>
								<img src='<?= Url::to(["../news/$news_object->image_name"]) ?>' alt="" title="News Image">
							</div>
							<div class="blog-content">
								<h4 class="blog-title" style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><a href='<?= Url::to(["en/news?id=$news_object->id"]) ?>'><?= $news_object["title_en"] ?></a></h4>
								<p style="overflow: hidden; white-space: nowrap; text-overflow: ellipsis;"><?php echo $news_object["content_en"]; ?></p>
							</div>
						</article>	
					</div>

		        	<?php
		        		}
		        	?>
					
				</div>
				
			</div>
			
			
			<!-- Sidebar
			============================================= -->
			<aside class="col-md-6">
				
					<h2 class="bordered light">Health <span>Advise</span> <a href="<?= Url::to(['en/all-articles']) ?>" class="btn btn-default btn-xs pull-right">All</a></h2>
					<p>The Basics to Practice Every Day<br>He who has health has hope, and he who has hope has everything. "Arabian Proverb"</p>
					<div class="panel-group" id="accordion">
					  
						<?php
							$index = 0;
			        		foreach($articles as $article) {
			        	?>

						<div class="panel panel-default">
							<div class="panel-heading">
							  <h4 class="panel-title">
								<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $index; ?>">
								  <span><i class="fa fa-plus"></i></span><?php echo $article["title_en"]; ?>
								</a>
							  </h4>
							</div>
							<div id="collapse<?php echo $index; ?>" class="panel-collapse collapse">
							  <div class="panel-body">
							   <?php echo $article["content_en"]; ?>
							  </div>
							</div>
						  </div>

			        	<?php
			        			$index++;
			        		}
			        	?>
					  
					</div>
			
			</aside>
			
		</div>
		
	</div>
	
</section>



<!-- App Store
============================================= -->
<section class="medicom-app" data-stellar-background-ratio="0.3">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<img src="<?= Url::to(['en/images/mobile-hand.png']) ?>" class="app-img" alt="" title="">
			</div>
			<div class="col-md-7">
				<div class="medicom-app-content">
					<h1>Future Care Clinics App Available</h1>
					<p class="lead">Now you can download our apps for smartphones
					<span>AVAILABLE ON STORES</span>
					</p>
					<ul class="list-unstyled app-buttons">
						<li><a href="#."><img src="<?= Url::to(['en/images/app-store-btn.png']) ?>" alt="" title="App Store"></a></li>
						<li><a href="#."><img src="<?= Url::to(['en/images/play-store-btn.png']) ?>" alt="" title="Google App"></a></li>
					</ul>
					</div>
			</div>
		</div>
	</div>
</section>




<!-- Testimonials
============================================= -->
<section class="testimonials bg-white text-center">
	
	<div class="container">
		
		<h2>Clients</h2>
		<div class="row">
	
			<div class="col-md-12">
			
				<div id="carousel-testimonials" class="owl-carousel slide testimonials-carousel">

					<!-- Item -->
					<?php
					$images = glob("../clients/*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE);
			        foreach($images as $img) {
			            echo '<div class="item" style="vertical-alig: middle;">';
						echo '<div class="testimonials-patient-detail">';
						echo '<img src="' . Url::to([$img]) . '" class="img-thumbnail" alt="" title="">';
						echo '</div>';
						echo '</div>';
			        }
			        ?>
					<!-- End Item -->
					
				</div>

			</div>
	
		</div>
	
	</div>
	
</section>