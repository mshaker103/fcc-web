<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clinic".
 *
 * @property integer $id
 * @property string $title_en
 * @property string $title_ar
 * @property string $content_en
 * @property string $content_ar
 * @property string $created_at
 */
class Clinic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clinic';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title_en', 'title_ar', 'content_en', 'content_ar'], 'required'],
            [['content_en', 'content_ar'], 'string'],
            [['created_at'], 'safe'],
            [['title_en', 'title_ar'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title_en' => 'Title En',
            'title_ar' => 'Title Ar',
            'content_en' => 'Content En',
            'content_ar' => 'Content Ar',
            'created_at' => 'Created At',
        ];
    }
}
