<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctor".
 *
 * @property integer $id
 * @property string $name_en
 * @property string $name_ar
 * @property string $job_position_en
 * @property string $job_position_ar
 * @property string $working_hours_en
 * @property string $working_hours_ar
 * @property string $ext_doctor_en
 * @property string $ext_doctor_ar
 * @property string $image_name
 * @property string $created_at
 */
class Doctor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'doctor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_en', 'name_ar', 'job_position_en', 'job_position_ar', 'working_hours_en', 'working_hours_ar', 'ext_doctor_en', 'ext_doctor_ar', 'image_name'], 'required'],
            [['working_hours_en', 'working_hours_ar'], 'string'],
            [['created_at'], 'safe'],
            [['name_en', 'name_ar', 'job_position_en', 'job_position_ar', 'image_name'], 'string', 'max' => 250],
            [['ext_doctor_en', 'ext_doctor_ar'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_en' => 'Name En',
            'name_ar' => 'Name Ar',
            'job_position_en' => 'Job Position En',
            'job_position_ar' => 'Job Position Ar',
            'working_hours_en' => 'Working Hours En',
            'working_hours_ar' => 'Working Hours Ar',
            'ext_doctor_en' => 'Ext Doctor En',
            'ext_doctor_ar' => 'Ext Doctor Ar',
            'image_name' => 'Image Name',
            'created_at' => 'Created At',
        ];
    }
}
