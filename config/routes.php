<?php
return [
	['class' => 'yii\rest\UrlRule', 'controller' => 'article'],
	['class' => 'yii\rest\UrlRule', 'controller' => 'news'],
	['class' => 'yii\rest\UrlRule', 'controller' => 'clinic'],
	['class' => 'yii\rest\UrlRule', 'controller' => 'department'],
	['class' => 'yii\rest\UrlRule', 'controller' => 'doctor'],
	['class' => 'yii\rest\UrlRule', 'controller' => 'device'],
	'banners' => 'api/banners',
	'clients' => 'api/clients',
	'gallery' => 'api/gallery',
];
