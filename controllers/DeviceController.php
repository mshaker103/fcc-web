<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\base\Exception;

use app\models\Device;


class DeviceController extends ActiveController
{
    public $modelClass = 'app\models\Device';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['create']);
        return $actions;
    }


    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(isset($_POST['ios_token'])) {
            $model = new Device();
            $model->ios_token = $_POST['ios_token'];
            if (!$model->save()) {
            	return array('status' => false, 'message' => json_encode($model->getErrors()));
            }
        } else {
        	return array('status' => false, 'message' => "No POST variables sent");
        }

        return array('status' => true, 'message' => "Device token saved successfully");
    }


    public static function sendPushNotifications($messageContent) {
    	// $apnsHost = 'gateway.sandbox.push.apple.com';
        // $apnsCert = Yii::$app->basePath . '/apns-dev.pem';

        $apnsHost = 'gateway.push.apple.com';
        $apnsCert = Yii::$app->basePath . '/apns-production.pem';
		
		$apnsPort = 2195;
		$passphrase = 'Future_Care_Clinics';

		$streamContext = stream_context_create();
		stream_context_set_option($streamContext, 'ssl', 'local_cert', $apnsCert);
		stream_context_set_option($streamContext, 'ssl', 'passphrase', $passphrase);
		

		$payload['aps'] = array('alert' => $messageContent, 'badge' => 1, 'sound' => 'default');

		$output = json_encode($payload);

		$devices = Device::find()->all();
		foreach ($devices as $device) {
            $apns = stream_socket_client('ssl://' . $apnsHost . ':' . $apnsPort, $error, $errorString, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $streamContext);
            if (!$apns) {
                echo '<script type="text/javascript">alert("Failed to connect to APNS with error: ' . $errorString . '"); </script>';
                return;
            }
			$token = pack('H*', str_replace(' ', '', $device->ios_token));
			$apnsMessage = chr(0) . chr(0) . chr(32) . $token . chr(0) . chr(strlen($output)) . $output;
            fwrite($apns, $apnsMessage, strlen($apnsMessage));
			// echo $device->ios_token . " - " . fwrite($apns, $apnsMessage, strlen($apnsMessage)) . "<br>";
            @socket_close($apns);
            fclose($apns);
		}
		
    }
}