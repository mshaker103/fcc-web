<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Article;
use app\models\News;
use app\models\Clinic;
use app\models\Department;
use app\models\Doctor;
use app\models\Visit;


class ArController extends Controller
{

    public function actionHome()
    {
        $visit = new Visit();
        $visit->save();

        $this->layout = 'ar';

        $news = News::find()->limit(4)->orderBy(['id' => SORT_DESC])->all();
        $articles = Article::find()->limit(10)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('Home', [
            'news' => $news,
            'articles' => $articles,
        ]);
    }


    public function actionClinics()
    {
        $this->layout = 'ar';

        $clinics = Clinic::find()->all();
        return $this->render('Clinics', [
            'clinics' => $clinics
        ]);
    }


    public function actionDepartments()
    {
        $this->layout = 'ar';

        $departments = Department::find()->all();
        return $this->render('Departments', [
            'departments' => $departments
        ]);
    }


    public function actionGallery()
    {
        $this->layout = 'ar';
        return $this->render('Gallery');
    }


    public function actionAbout()
    {
        $this->layout = 'ar';
        $doctors = Doctor::find()->all();
        return $this->render('About', [
            'doctors' => $doctors
        ]);
    }


    public function actionContact()
    {
        $this->layout = 'ar';
        return $this->render('Contact');
    }


    public function actionAllNews()
    {
        $this->layout = 'ar';

        $news = News::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('AllNews', [
            'news' => $news,
        ]);
    }


    public function actionNews()
    {
        if (isset($_GET['id']))
        {
            $this->layout = 'ar';

            $news = News::find()->where(["id" => $_GET['id']])->one();
            return $this->render('News', [
                'news' => $news,
            ]);
        }
        
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionAllArticles()
    {
        $this->layout = 'ar';

        $articles = Article::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('AllArticles', [
            'articles' => $articles,
        ]);
    }
    
}
