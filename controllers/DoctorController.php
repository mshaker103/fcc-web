<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\base\Exception;

use app\models\Doctor;


class DoctorController extends ActiveController
{
    public $modelClass = 'app\models\Doctor';
}