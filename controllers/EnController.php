<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

use app\models\Article;
use app\models\News;
use app\models\Clinic;
use app\models\Department;
use app\models\Doctor;
use app\models\Visit;


class EnController extends Controller
{

    public function actionHome()
    {
        $visit = new Visit();
        $visit->save();

        $this->layout = 'en';

        $news = News::find()->limit(4)->orderBy(['id' => SORT_DESC])->all();
        $articles = Article::find()->limit(10)->orderBy(['id' => SORT_DESC])->all();

        return $this->render('Home', [
            'news' => $news,
            'articles' => $articles,
        ]);
    }


    public function actionClinics()
    {
        $this->layout = 'en';

        $clinics = Clinic::find()->all();
        return $this->render('Clinics', [
            'clinics' => $clinics
        ]);
    }


    public function actionDepartments()
    {
        $this->layout = 'en';

        $departments = Department::find()->all();
        return $this->render('Departments', [
            'departments' => $departments
        ]);
    }


    public function actionGallery()
    {
        $this->layout = 'en';
        return $this->render('Gallery');
    }


    public function actionAbout()
    {
        $this->layout = 'en';
        $doctors = Doctor::find()->all();
        return $this->render('About', [
            'doctors' => $doctors
        ]);
    }


    public function actionContact()
    {
        $this->layout = 'en';
        return $this->render('Contact');
    }


    public function actionAllNews()
    {
        $this->layout = 'en';

        $news = News::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('AllNews', [
            'news' => $news,
        ]);
    }


    public function actionNews()
    {
        if (isset($_GET['id']))
        {
            $this->layout = 'en';

            $news = News::find()->where(["id" => $_GET['id']])->one();
            return $this->render('News', [
                'news' => $news,
            ]);
        }
        
        return $this->redirect(Yii::$app->request->referrer);
    }


    public function actionAllArticles()
    {
        $this->layout = 'en';

        $articles = Article::find()->orderBy(['id' => SORT_DESC])->all();
        return $this->render('AllArticles', [
            'articles' => $articles,
        ]);
    }
    
}
