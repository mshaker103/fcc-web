<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

use app\models\User;

/**
 * Site controller
 */
class ApiController extends Controller
{

    public $enableCsrfValidation = false;


    public function beforeAction($action)
    {
        if (in_array($action->id, ['incoming'])) {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [],
                'actions' => [
                    'incoming' => [
                        'Origin' => ['*'],
                        'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
                        'Access-Control-Request-Headers' => ['*'],
                        'Access-Control-Allow-Credentials' => null,
                        'Access-Control-Max-Age' => 86400,
                        'Access-Control-Expose-Headers' => [],
                    ],
                ],
            ],
        ];
    }


    public function actionBanners()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return array_map('basename', glob("../banners/*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE));
    }


    public function actionClients()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return array_map('basename', glob("../clients/*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE));
    }


    public function actionGallery()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return array_map('basename', glob("../gallery/*.{jpg,JPG,jpeg,JPEG,png,PNG}", GLOB_BRACE));
    }
}
