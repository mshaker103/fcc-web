<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\base\Exception;

use app\models\Article;


class ArticleController extends ActiveController
{
    public $modelClass = 'app\models\Article';
}