<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\base\Exception;

use app\models\News;


class NewsController extends ActiveController
{
    public $modelClass = 'app\models\News';
}