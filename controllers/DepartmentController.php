<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\base\Exception;

use app\models\Department;


class DepartmentController extends ActiveController
{
    public $modelClass = 'app\models\Department';
}