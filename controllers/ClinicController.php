<?php

namespace app\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\base\Exception;

use app\models\Clinic;


class ClinicController extends ActiveController
{
    public $modelClass = 'app\models\Clinic';
}