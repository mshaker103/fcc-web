<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\User;

use app\models\Article;
use app\models\News;
use app\models\Clinic;
use app\models\Department;
use app\models\Doctor;
use app\models\Visit;
use app\models\Preferences;


class AdminController extends Controller
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;

        /*if (\Yii::$app->user->isGuest) {
            return $this->redirect('login', 302);
        }*/

        return parent::beforeAction($action);
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => [
                            'logout',
                            'home',
                            'add-article',
                            'add-clinic',
                            'add-department',
                            'add-news',
                            'add-doctor',
                            'add-client',
                            'add-banner',
                            'add-gallery',
                            'edit-preferences',
                            'all-articles',
                            'all-clinics',
                            'all-departments',
                            'all-news',
                            'all-doctors',
                            'all-clients',
                            'all-banners',
                            'all-gallery',
                        ],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect('home', 302);
        }


        if(isset($_POST['User'])) {
            $username = $_POST['User']['username'];
            $password = $_POST['User']['password'];

            if ($username == 'admin' && $password == 'DPeMDhg9GF34zMJC') {
                $user = User::findByUsername($username);
                $res = Yii::$app->user->login($user, 3600*24*30);

                if ($res == true) {
                    return $this->redirect('home', 302);
                }
            }
        }

        $this->layout = 'empty';
        return $this->render('login');
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();
        return $this->redirect('login', 302);
    }


    public function actionHome()
    {
        // DeviceController::sendPushNotifications("Test Notification");

        $articles_count = Article::find()->count();
        $news_count = News::find()->count();
        $clinics_count = Clinic::find()->count();
        $departments_count = Department::find()->count();
        $doctors_count = Doctor::find()->count();
        $visits_count = Visit::find()->count();

        $clients_count = count(glob("../../clients/*.{jpg,png,gif}", GLOB_BRACE));
        $banners_count = count(glob("../../banners/*.{jpg,png,gif}", GLOB_BRACE));
        
        return $this->render('Home', [
            'articles_count' => $articles_count,
            'news_count' => $news_count,
            'clinics_count' => $clinics_count,
            'departments_count' => $departments_count,
            'doctors_count' => $doctors_count,
            'visits_count' => $visits_count,
            'clients_count' => $clients_count,
            'banners_count' => $banners_count,
        ]);
    }


    public function actionAddArticle()
    {
        $is_insert = false;
        $insert_success = false;
        $message = "";

        if (isset($_POST['Article']))
        {
            $is_insert = true;
            $model = new Article();
            $model->attributes = $_POST['Article'];
            if ($model->save()) {
                $insert_success = true;
                $message = "Saved Successfully!";
                DeviceController::sendPushNotifications("مقال جديد: " . $model->title_ar);
            } else {
                $message = json_encode($model->getErrors());
                // $message = "Error happed while insert the new data, Please try again!";
            }
        }

        return $this->render('AddArticle', ['is_insert'=>$is_insert,
            'insert_success'=>$insert_success,
            'message'=>$message]);
    }


    public function actionAddClinic()
    {
        $is_insert = false;
        $insert_success = false;
        $message = "";

        if (isset($_POST['Clinic']))
        {
            $is_insert = true;
            $model = new Clinic();
            $model->attributes = $_POST['Clinic'];
            if ($model->save()) {
                $insert_success = true;
                $message = "Saved Successfully!";
            } else {
                $message = json_encode($model->getErrors());
                // $message = "Error happed while insert the new data, Please try again!";
            }
        }

        return $this->render('AddClinic', ['is_insert'=>$is_insert,
            'insert_success'=>$insert_success,
            'message'=>$message]);
    }


    public function actionAddDepartment()
    {
        $is_insert = false;
        $insert_success = false;
        $message = "";

        if (isset($_POST['Department']))
        {
            $is_insert = true;
            $model = new Department();
            $model->attributes = $_POST['Department'];
            if ($model->save()) {
                $insert_success = true;
                $message = "Saved Successfully!";
            } else {
                $message = json_encode($model->getErrors());
                // $message = "Error happed while insert the new data, Please try again!";
            }
        }

        return $this->render('AddDepartment', ['is_insert'=>$is_insert,
            'insert_success'=>$insert_success,
            'message'=>$message]);
    }


    public function actionAddNews()
    {
        $is_insert = false;
        $insert_success = false;
        $message = "";


        if (isset($_POST['News']))
        {
            $is_insert = true;
            $model = new News();
            $model->attributes = $_POST['News'];

            $target_dir = "../news/";
            $ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
            $model->image_name = date('Y-m-d|H-i-s', strtotime("now"))  . '.' . $ext;
            $target_file = $target_dir . $model->image_name;
            

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                if ($model->save()) {
                    $insert_success = true;
                    $message = "Saved Successfully!";
                    DeviceController::sendPushNotifications("خبر جديد: " . $model->title_ar);
                } else {
                    $message = json_encode($model->getErrors());
                    // $message = "Error happed while insert the new data, Please try again!";
                }
            } else {
                $message = "Sorry, there was an error uploading your file.";
            }

        }

        return $this->render('AddNews', ['is_insert'=>$is_insert,
            'insert_success'=>$insert_success,
            'message'=>$message]);
    }


    public function actionAddDoctor()
    {
        $is_insert = false;
        $insert_success = false;
        $message = "";


        if (isset($_POST['Doctor']))
        {
            $is_insert = true;
            $model = new Doctor();
            $model->attributes = $_POST['Doctor'];

            $target_dir = "../doctors/";
            $ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
            $model->image_name = date('Y-m-d|H-i-s', strtotime("now"))  . '.' . $ext;
            $target_file = $target_dir . $model->image_name;
            

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
                if ($model->save()) {
                    $insert_success = true;
                    $message = "Saved Successfully!";
                } else {
                    $message = json_encode($model->getErrors());
                    // $message = "Error happed while insert the new data, Please try again!";
                }
            } else {
                $message = "Sorry, there was an error uploading your file.";
            }

        }

        return $this->render('AddDoctor', ['is_insert'=>$is_insert,
            'insert_success'=>$insert_success,
            'message'=>$message]);
    }


    public function actionAddClient()
    {
        return $this->render('AddClient');
    }


    public function actionAddBanner()
    {
        return $this->render('AddBanner');
    }


    public function actionAddGallery()
    {
        return $this->render('AddGallery');
    }


    public function actionEditPreferences()
    {
        $is_insert = false;
        $insert_success = false;
        $message = "";

        $showVisitorsModel = Preferences::find()->where(['id' => 'show_visitors'])->one();

        if (isset($_POST['Preferences']))
        {
            $is_insert = true;
            $showVisitorsModel->value = $_POST['Preferences']['show_visitors'];
            if ($showVisitorsModel->save()) {
                $insert_success = true;
                $message = "Saved Successfully!";
            } else {
                $message = json_encode($showVisitorsModel->getErrors());
                // $message = "Error happed while insert the new data, Please try again!";
            }
        }

        return $this->render('EditPreferences', ['showVisitors' => $showVisitorsModel->value,
            'is_insert' => $is_insert,
            'insert_success' => $insert_success,
            'message' => $message]);
    }


    public function actionAllArticles()
    {
        if (isset($_POST["deleteId"])) {
            $obj = Article::find()->where(['id' => $_POST["deleteId"]])->one();
            $obj->delete();
        }

        $articles = Article::find()->all();
        return $this->render('AllArticles', ['articles'=>$articles]);
    }


    public function actionAllClinics()
    {
        if (isset($_POST["deleteId"])) {
            $obj = Clinic::find()->where(['id' => $_POST["deleteId"]])->one();
            $obj->delete();
        }

        $clinics = Clinic::find()->all();
        return $this->render('AllClinics', ['clinics'=>$clinics]);
    }


    public function actionAllDepartments()
    {
        if (isset($_POST["deleteId"])) {
            $obj = Department::find()->where(['id' => $_POST["deleteId"]])->one();
            $obj->delete();
        }

        $departments = Department::find()->all();
        return $this->render('AllDepartments', ['departments'=>$departments]);
    }


    public function actionAllNews()
    {
        if (isset($_POST["deleteId"])) {
            $obj = News::find()->where(['id' => $_POST["deleteId"]])->one();
            $obj->delete();
        }

        $news = News::find()->all();
        return $this->render('AllNews', ['news'=>$news]);
    }


    public function actionAllDoctors()
    {
        if (isset($_POST["deleteId"])) {
            $obj = Doctor::find()->where(['id' => $_POST["deleteId"]])->one();
            $obj->delete();
        }

        $doctors = Doctor::find()->all();
        return $this->render('AllDoctors', ['doctors'=>$doctors]);
    }


    public function actionAllClients()
    {
        if (isset($_POST["deleteFileName"])) {
            unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'clients' . DIRECTORY_SEPARATOR . $_POST["deleteFileName"]);
        }

        return $this->render('AllClients');
    }


    public function actionAllBanners()
    {
        if (isset($_POST["deleteFileName"])) {
            unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'banners' . DIRECTORY_SEPARATOR . $_POST["deleteFileName"]);
        }

        return $this->render('AllBanners');
    }


    public function actionAllGallery()
    {
        if (isset($_POST["deleteFileName"])) {
            unlink(Yii::$app->basePath . DIRECTORY_SEPARATOR . 'gallery' . DIRECTORY_SEPARATOR . $_POST["deleteFileName"]);
        }

        return $this->render('AllGallery');
    }
}
