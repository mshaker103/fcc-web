<?php

$ds          = DIRECTORY_SEPARATOR;  //1
$storeFolder = '../../gallery';   //2

if (!empty($_FILES)) {
    $tempFile = $_FILES['file']['tmp_name'];          //3
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  //4
    if (!file_exists($targetPath)) {
    	mkdir($targetPath, 0777, true);
	}
	
	$ext = pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION);
    $image_name = date('Y-m-d|H-i-s', strtotime("now"))  . '.' . $ext;
    $targetFile =  $targetPath. $image_name;  //5
    move_uploaded_file($tempFile, $targetFile); //6
}

?>